/*
 * Toy.cpp

 *
 *  Created on: 30/12/2015
 *      Author: C
 */

#include "Toy.h"
#include <iostream>
#include <string>
#include <sstream>
using namespace std;

Toy::Toy(double price,string brand,string name,string quantity):Item(price),brand(brand),name(name),quantity(quantity){}

string Toy::show(){
	string tic = Item::show("Juguete: ");
	stringstream priceStr;
	priceStr << Item::getPrice()*IVA;
	tic += quantity + " " + name + " " + priceStr.str();
	return tic;

}

double Toy::pvp(){
	double price = Item::getPrice();
	stringstream num (quantity);
	int quanti;
	num >> quanti;
	return price*IVA*quanti; //multiplico el precio sin iva por el iva y por la cantidad comprada
}


