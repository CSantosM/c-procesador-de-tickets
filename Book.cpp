/*
 * Book.cpp

 *
 *  Created on: 26/12/2015
 *      Author: C
 */

using namespace std;
#include <iostream>
#include <string>
#include <sstream>
#include "Book.h"

Book::Book(double price,string title,string author):Item(price),title(title),author(author){}

string Book::show(){
	string tic = Item::show("Libro: ");
	stringstream priceStr;
	priceStr << Item::getPrice()*IVA; //casting de double a string
	tic += title + " " + priceStr.str();
	return tic;
}

double Book::pvp(){
	double price = Item::getPrice();
	return price*IVA;
}

