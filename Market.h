/*
 * Market.h
 *
 *  Created on: 2/1/2016
 *      Author: C
 */

#ifndef MARKET_H_
#define MARKET_H_
#include "Item.h"
#include <string>
using namespace std;

class Market: public Item{

public:
	Market(double price,string name,string quantity);
	string show();
	double pvp();

private:
	string name,quantity;
	const static double IVA = 1.08;
};




#endif /* MARKET_H_ */
