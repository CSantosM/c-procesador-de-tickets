/*
 * ItemProcessor.h
 *
 *  Created on: 26/12/2015
 *      Author: C
 */

#ifndef ITEMPROCESSOR_H_
#define ITEMPROCESSOR_H_

using namespace std;
#include <string>
#include <list>
#include "Item.h"
#include "Book.h"
#include "Toy.h"
#include "Market.h"

class ItemProcessor{

public:
	void load(const char *filename);
	double pvp ();
	string generateTicket();
	~ItemProcessor();


private:
	int length; // numero de items
	Item** array; //array que contendra los items


};





#endif /* ITEMPROCESSOR_H_ */
