/*
 * ItemProcessor.cpp
 *
 *  Created on: 26/12/2015
 *      Author: C
 */


#include "ItemProcessor.h"
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

void ItemProcessor::load(const char *filename){
	ifstream file (filename); //uso ifstream para leer de fichero (input.txt en este caso)
		int type = 0;
		string title,author,name,brand,quanti;
		double price;
		if (file!=NULL){

			file >> this->length; //numero de elementos (primera linea del fichero)
			this->array = new Item* [this->length];
			for(int i=0; i<this->length;i++){
				file >> type;
				switch(type){ //Dependiento del tipo de articulo, creo un objeto diferente
					case 1:{
						file >> title >> author >> price;
						Book* book =new Book(price,title,author);
						this->array[i] = book;
						break;
					}
					case 2:{
						file >> name >> quanti >> price;
						Market* market = new Market(price,name,quanti);
						this->array[i] = market;
						break;
					}
					case 3:{
						file >> brand >> name >> quanti >> price;
						Toy* toy = new Toy(price,brand,name,quanti);
						this->array[i] = toy;
						break;
					}
				}
			}
			file.close();
		}
		else{
			cout<< "ERROR: El archivo especificado no existe, reviselo por favor.\n";

		}

}


double ItemProcessor::pvp(){
	double total= 0.0;
	for(int i=0; i<this->length;i++){ //Recorro el array usando el metodo polimórfico pvp() y calculando el precio total.
		 total += this->array[i]->pvp();
	}
	cout << "Total: ";
	return total;

}

string ItemProcessor::generateTicket(){
	stringstream ss;
	string tic = "\n *************************** TICKET ***************************\n";
	double total = 0.0;

	for (int i=0; i<this->length; i++){
		tic += this->array[i]->show() +"\n";
		total += this->array[i]->pvp();
	}
	ss << total;
	return tic+"\n" + "Total: " + ss.str() + "\n **************************************************************";


}

ItemProcessor::~ItemProcessor(){
	if (this->array!=NULL){
		for(int i=0; i<this->length;i++){ // recorro el arrayay borrando cada objeto
			delete this->array[i];
		}
		delete [] this->array; //Borro el arrayay.
	}

}
