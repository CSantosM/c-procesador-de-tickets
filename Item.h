/*
 * Item.h
 *
 *  Created on: 26/12/2015
 *      Author: C
 */

#ifndef ITEM_H_
#define ITEM_H_
#include <string>
using namespace std;
class Item{

public:

	Item(double price);
	virtual string show();
	string show(string category);
	virtual double pvp();
	virtual ~Item(){};

private:
	double price;
protected:
	double getPrice();
};




#endif /* ITEM_H_ */
