/*
 * Market.cpp
 *
 *  Created on: 2/1/2016
 *      Author: C
 */

#include"Market.h"
#include <string>
using namespace std;
#include <iostream>
#include <sstream>

Market::Market(double price,string name,string quantity):Item(price),name(name),quantity(quantity){}

string Market::show(){
	stringstream priceStr;
	string tic = Item::show("Supermercado: ");
	priceStr << Item::getPrice()*IVA; //casting a string
	tic += quantity + " " + name + " " + priceStr.str();
	return tic;
}

double Market::pvp(){
	double price = Item::getPrice();
	stringstream num (quantity);
	int quanti;
	num >> quanti;
	return price*IVA*quanti; //multiplico el precio sin iva por el iva y por la cantidad comprada
}

