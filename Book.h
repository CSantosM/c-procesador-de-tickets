/*
 * Books.h
 *
 *  Created on: 26/12/2015
 *      Author: C
 */

#ifndef BOOK_H_
#define BOOK_H_
#include <string>
#include "Item.h"
using namespace std;

class Book: public Item{


public:
	Book(double price, string title, string author);
	string show();
	double pvp();

private:
	string title;
	string author;
	const static double IVA = 1.16;
};



#endif /* BOOK_H_ */
