/*
 * Toys.h
 *
 *  Created on: 26/12/2015
 *      Author: C
 */

#ifndef TOY_H_
#define TOY_H_
#include "Item.h"
#include<string>
using namespace std;

class Toy: public Item{

public:
	Toy(double price,string brand,string name, string quantity);
	string show();
	double pvp();

private:
	string brand,name,quantity;
	const static double IVA = 1.21;

};




#endif /* TOY_H_ */
